provider "google" {
  credentials = file("/var/lib/jenkins/sc.json")
  project     = "nbn-project-244123"
  region      = "us-central1"
}